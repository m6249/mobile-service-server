import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { PhoneBrandModel } from './phone.brand.model';
import { CreateBrandDto } from './dto/create.brand.dto';
import { ProgramBrandModel } from './program.brand.model';
import { GameBrandModel } from './game.brand.model';
import { DeleteBrandDto } from './dto/delete.brand.dto';

@Injectable()
export class BrandService {
  constructor(
    @InjectModel(PhoneBrandModel)
    private phoneBrandRepository: typeof PhoneBrandModel,
    @InjectModel(ProgramBrandModel)
    private programBrandRepository: typeof ProgramBrandModel,
    @InjectModel(GameBrandModel)
    private gameBrandRepository: typeof GameBrandModel,
  ) {}

  async createBrand({ name, type }: CreateBrandDto) {
    switch (type) {
      case 'phone':
        return await this.phoneBrandRepository.create({ name });
      case 'program':
        return await this.programBrandRepository.create({ name });
      case 'game':
        return await this.gameBrandRepository.create({ name });
    }
  }

  async getAllBrand(type: string) {
    switch (type) {
      case 'phone':
        return await this.phoneBrandRepository.findAll();
      case 'program':
        return await this.programBrandRepository.findAll();
      case 'game':
        return await this.gameBrandRepository.findAll();
    }
  }

  async deleteBrand({ type, id }: DeleteBrandDto) {
    switch (type) {
      case 'phone':
        return await this.phoneBrandRepository.destroy({ where: { id } });
      case 'game':
        return await this.gameBrandRepository.destroy({ where: { id } });
      case 'program':
        return await this.programBrandRepository.destroy({ where: { id } });
    }
  }
}

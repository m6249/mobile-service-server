export class CreateGameDto {
  readonly id: number;
  readonly brandId: number;
  readonly title: string;
  readonly price: number;
  readonly discount: boolean;
  readonly term: string;
  readonly instructionTitle: string;
  readonly instructionStep: [string];
}

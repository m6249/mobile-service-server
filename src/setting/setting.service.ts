import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { ServiceModel } from './service.model';
import { CreateServiceDto } from './dto/create.service.dto';

@Injectable()
export class SettingService {
  constructor(
    @InjectModel(ServiceModel) private serviceRepository: typeof ServiceModel,
  ) {}

  async create(dto: CreateServiceDto) {
    const service = await this.serviceRepository.create(dto);
    return service;
  }

  async getAll() {
    const services = await this.serviceRepository.findAll();
    return services;
  }

  async deleteService(id: number) {
    const service = await this.serviceRepository.destroy({ where: { id } });
    return service;
  }
}

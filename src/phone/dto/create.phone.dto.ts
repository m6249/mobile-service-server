export class CreatePhoneDto {
  readonly id: number;
  readonly brandId: number;
  readonly title: string;
  readonly price: number;
  readonly discount: boolean;
  readonly href?: string | null;
  readonly term: string;
  readonly instructionTitle: string;
  readonly instructionStep: [string];
  readonly downloadAppUrl: string;
}

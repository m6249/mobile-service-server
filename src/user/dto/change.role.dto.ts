export class ChangeRoleDto {
  readonly email: string;
  readonly role: number;
}

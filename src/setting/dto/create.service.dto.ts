export class CreateServiceDto {
  readonly title: string;
  readonly items: [string];
}
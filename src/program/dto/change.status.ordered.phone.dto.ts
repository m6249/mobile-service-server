export class ChangeStatusOrderedPhoneDto {
  readonly id: number;
  readonly userId: number;
  readonly price: number;
  readonly status: string;
}

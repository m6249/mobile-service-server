import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { ProgramModel } from './program.model';
import { CreateProgramDto } from './dto/create.program.dto';
import { BrandService } from '../brand/brand.service';
import { ProgramOrderModel } from './program.order.model';
import { UserService } from '../user/user.service';
import { AuthService } from '../auth/auth.service';
import { CreateOrderProgramDto } from './dto/create.order.program.dto';
import { ChangeStatusOrderedPhoneDto } from './dto/change.status.ordered.phone.dto';

@Injectable()
export class ProgramService {
  constructor(
    @InjectModel(ProgramModel)
    private programRepository: typeof ProgramModel,
    @InjectModel(ProgramOrderModel)
    private programOrderRepository: typeof ProgramOrderModel,
    private brandService: BrandService,
    private userService: UserService,
    private authService: AuthService,
  ) {}

  async create(dto: CreateProgramDto) {
    const program = await this.programRepository.create(dto);
    return program;
  }

  async getOne(id: number) {
    const program = await this.programRepository.findOne({ where: { id } });
    return program;
  }

  async getAll() {
    const programBrands = await this.brandService.getAllBrand('program');
    const response = await this.generateResponse(
      programBrands,
      this.programRepository,
    );
    return response;
  }

  async getByBrandId(id: number) {
    const programs = await this.programRepository.findAll({
      where: { brandId: id },
      order: [['id', 'DESC']],
    });
    return programs;
  }

  async generateResponse(brands, repository) {
    const response = [];
    for (let i = 0; i < brands.length; i++) {
      response.push({
        title: brands[i].name,
        items: await repository.findAll({
          where: { brandId: brands[i].id },
        }),
      });
    }
    return response;
  }

  async orderProgram(dto: CreateOrderProgramDto) {
    const user = await this.userService.decrementSum(dto.userId, dto.price);
    const orderProgram = await this.programOrderRepository.create(dto);
    const { token } = await this.authService.generateToken(user);
    return {
      orderProgram,
      token,
    };
  }

  async getMyPrograms(id) {
    if (id) {
      const myPrograms = await this.programOrderRepository.findAll({
        where: { userId: id },
        order: [['id', 'DESC']],
      });
      return myPrograms;
    }
    throw new HttpException('userID is not found', HttpStatus.NOT_FOUND);
  }

  async updateProgram(dto: CreateProgramDto) {
    await this.programRepository.update({ ...dto }, { where: { id: dto.id } });
    const program = await this.programRepository.findOne({
      where: { id: dto.id },
    });
    return program;
  }

  async deleteProgram(id) {
    if (id) {
      const program = await this.programRepository.destroy({ where: { id } });
      return program;
    }
  }

  async getOrderedPrograms() {
    const orderedPrograms = await this.programOrderRepository.findAll({
      order: [['id', 'DESC']],
    });
    return orderedPrograms;
  }

  async changeStatus(dto: ChangeStatusOrderedPhoneDto) {
    if (dto.status === 'REJECTED') {
      await this.userService.incrementSum(dto.userId, dto.price);
    }
    await this.programOrderRepository.update(
      { status: dto.status },
      { where: { id: dto.id } },
    );
    const program = this.programOrderRepository.findOne({
      where: { id: dto.id },
    });
    return program;
  }

  async getIncome() {
    const programPrices = await this.programOrderRepository.findAll({
      attributes: ['price', 'status'],
    });
    let income = 0;
    await programPrices.forEach((value) => {
      income += value.status === 'SUCCESS' ? value.price : 0;
    });
    return income;
  }
}

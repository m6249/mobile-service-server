import { Module } from '@nestjs/common';
import { BrandController } from './brand.controller';
import { BrandService } from './brand.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { PhoneBrandModel } from './phone.brand.model';
import { AuthModule } from '../auth/auth.module';
import { ProgramBrandModel } from './program.brand.model';
import { GameBrandModel } from './game.brand.model';

@Module({
  controllers: [BrandController],
  providers: [BrandService],
  imports: [
    SequelizeModule.forFeature([
      PhoneBrandModel,
      ProgramBrandModel,
      GameBrandModel,
    ]),
    AuthModule,
  ],
  exports: [BrandService],
})
export class BrandModule {}

import { CreateProgramDto } from './create.program.dto';
import { CreateUserDto } from '../../user/dto/create.user.dto';

export class CreateOrderProgramDto {
  readonly program: CreateProgramDto;
  readonly userId: number;
  readonly user: CreateUserDto;
  readonly email: string;
  readonly userName: string;
  readonly price: number;
  readonly status: string;
}

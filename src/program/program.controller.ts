import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ProgramService } from './program.service';
import { CreateProgramDto } from './dto/create.program.dto';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { CreateOrderProgramDto } from './dto/create.order.program.dto';
import { RoleAuthGuard } from '../auth/role.auth.guard';
import { ChangeStatusOrderedPhoneDto } from "./dto/change.status.ordered.phone.dto";

@Controller('/program')
export class ProgramController {
  constructor(private programService: ProgramService) {}

  @Get('/order')
  getOrderedPrograms() {
    return this.programService.getOrderedPrograms();
  }

  @Get('/:id')
  getOne(@Param('id') id: number) {
    return this.programService.getOne(id);
  }


  @Get('/brand/:id')
  getByBrandId(@Param('id') id: number) {
    return this.programService.getByBrandId(id);
  }

  @Get()
  getAll() {
    return this.programService.getAll();
  }

  @Post('/change_status')
  changeStatus(@Body() dto: ChangeStatusOrderedPhoneDto) {
    return this.programService.changeStatus(dto);
  }

  @Post()
  create(@Body() dto: CreateProgramDto) {
    return this.programService.create(dto);
  }

  @Post('/order')
  @UseGuards(JwtAuthGuard)
  orderPhone(@Body() dto: CreateOrderProgramDto) {
    return this.programService.orderProgram(dto);
  }

  @Get('/my/:id')
  @UseGuards(JwtAuthGuard)
  getMyProgram(@Param('id') id) {
    return this.programService.getMyPrograms(id);
  }

  @Post('/update')
  @UseGuards(RoleAuthGuard)
  updateProgram(@Body() dto: CreateProgramDto) {
    return this.programService.updateProgram(dto);
  }

  @Post('/delete')
  @UseGuards(RoleAuthGuard)
  deleteProgram(@Body('id') id) {
    return this.programService.deleteProgram(id);
  }
}

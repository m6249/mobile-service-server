import { Column, DataType, Model, Table } from 'sequelize-typescript';
import { CreateUserDto } from '../user/dto/create.user.dto';
import { CreateProgramDto } from './dto/create.program.dto';

interface ProgramOrderCreationAtr {
  program: CreateProgramDto;
  userId: number;
  user: CreateUserDto;
  email: string;
  userName: string;
  price: number;
  status: string;
}

@Table({ tableName: 'programOrders' })
export class ProgramOrderModel extends Model<
  ProgramOrderModel,
  ProgramOrderCreationAtr
> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @Column({ type: DataType.INTEGER, allowNull: false })
  userId: number;

  @Column({ type: DataType.JSON, allowNull: false })
  user: CreateUserDto;

  @Column({ type: DataType.JSON, allowNull: false })
  program: CreateProgramDto;

  @Column({ type: DataType.FLOAT, allowNull: false })
  price: number;

  @Column({ type: DataType.STRING, allowNull: false })
  email: string;

  @Column({ type: DataType.STRING, allowNull: false })
  userName: string;

  @Column({ type: DataType.STRING, defaultValue: 'PENDING' })
  status: string;
}

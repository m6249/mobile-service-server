export class CreateBrandDto {
  readonly name: string;
  readonly type: string;
}

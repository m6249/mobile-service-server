import { forwardRef, Module } from "@nestjs/common";
import { SequelizeModule } from '@nestjs/sequelize';
import { GameModel } from './game.model';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { BrandModule } from '../brand/brand.module';
import { GameService } from './game.service';
import { GameController } from './game.controller';
import { GameOrderModel } from './game.order.model';

@Module({
  providers: [GameService],
  controllers: [GameController],
  imports: [
    SequelizeModule.forFeature([GameModel, GameOrderModel]),
    UserModule,
    AuthModule,
    forwardRef(() => BrandModule),
  ],
  exports: [GameService],
})
export class GameModule {}

export class DeleteBrandDto {
  readonly id: number;
  readonly type: string;
}

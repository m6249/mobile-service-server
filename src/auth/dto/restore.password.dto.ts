export class RestorePasswordDto {
  readonly email: string;
  readonly password: string;
}
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function start() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
    // {
    // origin: ['http://office.mobile-service1.ru', 'http://mobile-service1.ru'],
    // },
  });
  const PORT = process.env.PORT || 5000;
  await app.listen(PORT, () => console.log(`Server worked on port ${PORT}`));
}

start();

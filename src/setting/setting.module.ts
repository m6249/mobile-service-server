import { Module } from '@nestjs/common';
import { SettingService } from './setting.service';
import { SettingController } from './setting.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { ServiceModel } from './service.model';

@Module({
  providers: [SettingService],
  controllers: [SettingController],
  imports: [SequelizeModule.forFeature([ServiceModel])],
})
export class SettingModule {}

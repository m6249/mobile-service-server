import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface BrandCreationAtr {
  name: string;
}

@Table({ tableName: 'programBrands' })
export class ProgramBrandModel extends Model<
  ProgramBrandModel,
  BrandCreationAtr
> {
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING, allowNull: false, unique: true })
  name: string;
}

export class CreateUserDto {
  readonly fullName: string;
  readonly discount;
  readonly email: string;
  readonly password: string;
  readonly phoneNumber: string;
  readonly country: string;
}

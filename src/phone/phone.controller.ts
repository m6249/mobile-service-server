import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { PhoneService } from './phone.service';
import { CreatePhoneDto } from './dto/create.phone.dto';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { RoleAuthGuard } from '../auth/role.auth.guard';
import { CreatePhoneOrderDto } from './dto/create.phone.order.dto';
import { ChangeStatusOrderedPhoneDto } from "../program/dto/change.status.ordered.phone.dto";

@Controller('/phone')
export class PhoneController {
  constructor(private phoneService: PhoneService) {}

  @Get('/order')
  getOrderedPhones() {
    return this.phoneService.getOrderedPhones();
  }

  @Get('/:id')
  getOne(@Param('id') id) {
    return this.phoneService.getOne(id);
  }

  @Get()
  getAllWithBrand() {
    return this.phoneService.getAllWithBrand();
  }


  @Get('/brand/:id')
  get(@Param('id') id) {
    return this.phoneService.getPhonesByBrandId(id);
  }

  @Post()
  @UseGuards(JwtAuthGuard, RoleAuthGuard)
  create(@Body() dto: CreatePhoneDto) {
    return this.phoneService.create(dto);
  }

  @Post('/order')
  @UseGuards(JwtAuthGuard)
  orderPhone(@Body() dto: CreatePhoneOrderDto) {
    return this.phoneService.orderPhone(dto);
  }

  @Post('/change_status')
  changeStatus(@Body() dto: ChangeStatusOrderedPhoneDto) {
    return this.phoneService.changeStatus(dto);
  }

  @Get('/my/:id')
  @UseGuards(JwtAuthGuard)
  getMyPhones(@Param('id') id) {
    return this.phoneService.getMyPhones(id);
  }

  @Post('/update')
  @UseGuards(RoleAuthGuard)
  updatePhone(@Body() dto: CreatePhoneDto) {
    return this.phoneService.updatePhone(dto);
  }

  @Post('/delete')
  @UseGuards(RoleAuthGuard)
  deletePhone(@Body('id') id) {
    return this.phoneService.deletePhone(id);
  }
}

import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { GameService } from './game.service';
import { CreateGameDto } from './dto/create.game.dto';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { CreateOrderGameDto } from './dto/create.order.game.dto';
import { RoleAuthGuard } from '../auth/role.auth.guard';
import { ChangeStatusOrderedPhoneDto } from '../program/dto/change.status.ordered.phone.dto';

@Controller('/game')
export class GameController {
  constructor(private gameService: GameService) {}

  @Get('/order')
  getOrderedGames() {
    return this.gameService.getOrderedGames();
  }

  @Get('/:id')
  getOne(@Param('id') id: number) {
    return this.gameService.getOne(id);
  }

  @Get('/brand/:id')
  getByBrandId(@Param('id') id: number) {
    return this.gameService.getByBrandId(id);
  }

  @Get()
  getAll() {
    return this.gameService.getAll();
  }

  @Post()
  create(@Body() dto: CreateGameDto) {
    return this.gameService.create(dto);
  }

  @Post('/order')
  @UseGuards(JwtAuthGuard)
  orderPhone(@Body() dto: CreateOrderGameDto) {
    return this.gameService.orderGame(dto);
  }

  @Get('/my/:id')
  @UseGuards(JwtAuthGuard)
  getMyProgram(@Param('id') id) {
    return this.gameService.getMyGames(id);
  }

  @Post('/update')
  @UseGuards(RoleAuthGuard)
  updateGame(@Body() dto: CreateGameDto) {
    return this.gameService.updateGame(dto);
  }

  @Post('/change_status')
  changeStatus(@Body() dto: ChangeStatusOrderedPhoneDto) {
    return this.gameService.changeStatus(dto);
  }

  @Post('/delete')
  @UseGuards(RoleAuthGuard)
  deleteGame(@Body('id') id) {
    return this.gameService.deleteGame(id);
  }
}

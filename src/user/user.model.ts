import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface UserCreationAtr {
  fullName: string;
  email: string;
  password: string;
  phoneNumber: string;
  discount: number;
  country: string;
}

@Table({ tableName: 'users' })
export class UserModel extends Model<UserModel, UserCreationAtr> {
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
  })
  id: number;

  @Column({ type: DataType.STRING, allowNull: false })
  fullName: string;

  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  email: string;

  @Column({ type: DataType.STRING, allowNull: false })
  password: string;

  @Column({ type: DataType.STRING, allowNull: true })
  phoneNumber: string;

  @Column({ type: DataType.FLOAT, defaultValue: 0 })
  sum: number;

  @Column({ type: DataType.FLOAT, defaultValue: 0 })
  spentMoney: number;

  @Column({ type: DataType.FLOAT, defaultValue: 0 })
  discount: number;

  @Column({ type: DataType.STRING, allowNull: false })
  country: string;

  @Column({ type: DataType.INTEGER, defaultValue: 3 })
  role: number;
}

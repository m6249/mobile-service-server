import { Column, DataType, Model, Table } from 'sequelize-typescript';
import { CreateUserDto } from "../user/dto/create.user.dto";
import { CreatePhoneDto } from "./dto/create.phone.dto";

interface PhoneOrderCreationAtr {
  phone: CreatePhoneDto;
  userId: number;
  user: CreateUserDto;
  sn: string;
  price: number;
  status: string;
}

@Table({ tableName: 'phoneOrders' })
export class PhoneOrderModel extends Model<
  PhoneOrderModel,
  PhoneOrderCreationAtr
> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @Column({ type: DataType.INTEGER, allowNull: false })
  userId: number;

  @Column({ type: DataType.JSON, allowNull: false })
  user: CreateUserDto;

  @Column({ type: DataType.JSON, allowNull: false })
  phone: CreatePhoneDto;

  @Column({ type: DataType.FLOAT, allowNull: false })
  price: number;

  @Column({ type: DataType.STRING, allowNull: false })
  sn: string;

  @Column({ type: DataType.STRING, defaultValue: 'PENDING' })
  status: string;
}
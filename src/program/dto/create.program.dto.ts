export class CreateProgramDto {
  readonly id: number;
  readonly brandId: number;
  readonly title: string;
  readonly price: number;
  readonly discount: boolean;
  readonly term: string;
  readonly href?: string | null;
  readonly instructionTitle: string;
  readonly instructionStep: [string];
  readonly downloadAppUrl: string;
}

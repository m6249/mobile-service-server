import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule } from '@nestjs/config';
import { UserModel } from './user/user.model';
import { AuthModule } from './auth/auth.module';
import { PhoneModel } from './phone/phone.model';
import { MailerModule } from '@nestjs-modules/mailer';
import { PhoneBrandModel } from './brand/phone.brand.model';
import { BrandModule } from './brand/brand.module';
import { PhoneModule } from './phone/phone.module';
import { PhoneOrderModel } from './phone/phone.order.model';
import { ProgramModule } from './program/program.module';
import { ProgramModel } from './program/program.model';
import { ProgramOrderModel } from './program/program.order.model';
import { GameModel } from './game/game.model';
import { GameModule } from './game/game.module';
import { GameBrandModel } from './brand/game.brand.model';
import { GameOrderModel } from './game/game.order.model';
import { SumModule } from './sum/sum.module';
import { SettingModule } from './setting/setting.module';
import { ServiceModel } from './setting/service.model';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.${process.env.NODE_ENV}.env`,
    }),
    MailerModule.forRoot({
      transport: {
        service: 'gmail',
        auth: {
          user: process.env.EMAIL,
          pass: process.env.EMAIL_PASSWORD,
        },
      },
      defaults: {
        from: process.env.EMAIL,
      },
      template: {
        options: {
          strict: true,
        },
      },
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: +process.env.POSTGRES_PORT,
      username: process.env.POSTGRES_USER_NAME,
      password: process.env.POSTGES_PASSWORD,
      database: process.env.POSTGRES_DB,
      models: [
        UserModel,
        PhoneModel,
        PhoneBrandModel,
        GameBrandModel,
        PhoneOrderModel,
        GameOrderModel,
        ProgramModel,
        ProgramOrderModel,
        GameModel,
        ServiceModel,
      ],
      autoLoadModels: true,
    }),
    UserModule,
    AuthModule,
    PhoneModule,
    BrandModule,
    GameModule,
    ProgramModule,
    SumModule,
    SettingModule,
  ],
})
export class AppModule {}

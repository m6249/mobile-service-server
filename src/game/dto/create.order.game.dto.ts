import { CreateGameDto } from './create.game.dto';
import { CreateUserDto } from '../../user/dto/create.user.dto';

export class CreateOrderGameDto {
  readonly game: CreateGameDto;
  readonly userId: number;
  readonly user: CreateUserDto;
  readonly playerId: string;
  readonly nickName?: string;
  readonly price: number;
  readonly status: string;
}

import { Controller, Get, Query } from '@nestjs/common';
import { SumService } from './sum.service';

@Controller('sum')
export class SumController {
  constructor(private sumService: SumService) {}

  @Get()
  getAllIncome(@Query('type') type: string) {
    return this.sumService.getAllIncomeByType(type);
  }
}

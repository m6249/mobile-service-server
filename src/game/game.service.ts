import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { UserService } from '../user/user.service';
import { AuthService } from '../auth/auth.service';
import { GameModel } from './game.model';
import { CreateGameDto } from './dto/create.game.dto';
import { BrandService } from '../brand/brand.service';
import { GameOrderModel } from './game.order.model';
import { CreateOrderGameDto } from './dto/create.order.game.dto';
import { ChangeStatusOrderedPhoneDto } from '../program/dto/change.status.ordered.phone.dto';

@Injectable()
export class GameService {
  constructor(
    @InjectModel(GameModel)
    private gameRepository: typeof GameModel,
    @InjectModel(GameOrderModel)
    private gameOrderRepository: typeof GameOrderModel,
    private brandService: BrandService,
    private userService: UserService,
    private authService: AuthService,
  ) {}

  async create(dto: CreateGameDto) {
    const game = await this.gameRepository.create(dto);
    return game;
  }

  async getOne(id: number) {
    const game = await this.gameRepository.findOne({ where: { id } });
    return game;
  }

  async getAll() {
    const gameBrands = await this.brandService.getAllBrand('game');
    const response = await this.generateResponse(
      gameBrands,
      this.gameRepository,
    );
    return response;
  }

  async getByBrandId(id: number) {
    const programs = await this.gameRepository.findAll({
      where: { brandId: id },
      order: [['id', 'DESC']],
    });
    return programs;
  }

  async generateResponse(brands, repository) {
    const response = [];
    for (let i = 0; i < brands.length; i++) {
      response.push({
        title: brands[i].name,
        items: await repository.findAll({
          where: { brandId: brands[i].id },
          order: [['id', 'DESC']],
        }),
      });
    }
    return response;
  }

  async orderGame(dto: CreateOrderGameDto) {
    const user = await this.userService.decrementSum(dto.userId, dto.price);
    const orderGame = await this.gameOrderRepository.create(dto);
    const { token } = await this.authService.generateToken(user);
    return {
      orderGame: orderGame,
      token,
    };
  }

  async getMyGames(id) {
    if (id) {
      const myGames = await this.gameOrderRepository.findAll({
        where: { userId: id },
        order: [['id', 'DESC']],
      });
      return myGames;
    }
    throw new HttpException('userID is not found', HttpStatus.NOT_FOUND);
  }

  async updateGame(dto: CreateGameDto) {
    await this.gameRepository.update({ ...dto }, { where: { id: dto.id } });
    const game = await this.gameRepository.findOne({
      where: { id: dto.id },
    });
    return game;
  }

  async deleteGame(id) {
    if (id) {
      const game = await this.gameRepository.destroy({ where: { id } });
      return game;
    }
  }

  async getOrderedGames() {
    const orderedGames = await this.gameOrderRepository.findAll({
      order: [['id', 'DESC']],
    });
    return orderedGames;
  }

  async changeStatus(dto: ChangeStatusOrderedPhoneDto) {
    if (dto.status === 'REJECTED') {
      await this.userService.incrementSum(dto.userId, dto.price);
    }
    await this.gameOrderRepository.update(
      { status: dto.status },
      { where: { id: dto.id } },
    );
    const game = this.gameOrderRepository.findOne({ where: { id: dto.id } });
    return game;
  }

  async getIncome() {
    const programPrices = await this.gameOrderRepository.findAll({
      attributes: ['price', 'status'],
    });
    let income = 0;
    await programPrices.forEach((value) => {
      income += value.status === 'SUCCESS' ? value.price : 0;
    });
    return income;
  }
}

import { Injectable } from '@nestjs/common';
import { ProgramService } from '../program/program.service';
import { PhoneService } from '../phone/phone.service';
import { GameService } from '../game/game.service';

@Injectable()
export class SumService {
  constructor(
    private programService: ProgramService,
    private phoneService: PhoneService,
    private gameService: GameService,
  ) {}

  async getAllIncomeByType(type: string) {
    switch (type) {
      case 'programs':
        return await this.programService.getIncome();
      case 'phones':
        return await this.phoneService.getIncome();
      case 'games':
        return await this.gameService.getIncome();
      default:
        const programIncome = await this.programService.getIncome();
        const phoneIncome = await this.phoneService.getIncome();
        const gamesIncome = await this.gameService.getIncome();
        return programIncome + phoneIncome + gamesIncome;
    }
  }
}

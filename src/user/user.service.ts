import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { UserModel } from './user.model';
import { CreateUserDto } from './dto/create.user.dto';
import { RestorePasswordDto } from '../auth/dto/restore.password.dto';
import { ChangeRoleDto } from './dto/change.role.dto';
import { AddBalanceDto } from './dto/add.balance.dto';
import { AddDiscountDto } from './dto/add.discount.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(UserModel) private userRepository: typeof UserModel,
  ) {}

  async create(dto: CreateUserDto) {
    const user = await this.userRepository.create(dto);
    return user;
  }

  async getAll() {
    const users = await this.userRepository.findAll();
    return users;
  }

  async getUserByEmail(email: string) {
    const user = await this.userRepository.findOne({ where: { email } });
    return user;
  }

  async decrementSum(id: number, sum: number) {
    const updateUser = await this.userRepository.findOne({ where: { id } });
    await this.userRepository.update(
      { sum: updateUser.sum - sum, spentMoney: updateUser.spentMoney + sum },
      { where: { id } },
    );
    const user = await this.userRepository.findOne({
      where: { id: updateUser.id },
    });
    return user;
  }

  async incrementSum(id: number, sum: number) {
    const updateUser = await this.userRepository.findOne({ where: { id } });
    await this.userRepository.update(
      { sum: updateUser.sum + sum, spentMoney: updateUser.spentMoney - sum },
      { where: { id } },
    );
    const user = await this.userRepository.findOne({
      where: { id: updateUser.id },
    });
    return user;
  }

  async restorePassword(data: RestorePasswordDto, hashPassword) {
    await this.userRepository.update(
      { password: hashPassword },
      {
        where: { email: data.email },
      },
    );
    const user = await this.userRepository.findOne({
      where: { email: data.email },
    });
    return user;
  }

  async changeRole(dto: ChangeRoleDto) {
    await this.userRepository.update(
      { role: dto.role },
      { where: { email: dto.email } },
    );
    const user = await this.userRepository.findOne({
      where: { email: dto.email },
    });
    return user;
  }

  async addBalance(dto: AddBalanceDto) {
    const userUpdate = await this.userRepository.findOne({
      where: { email: dto.email },
    });
    await this.userRepository.update(
      { sum: userUpdate.sum + dto.balance },
      { where: { email: dto.email } },
    );
    const user = await this.userRepository.findOne({
      where: { email: dto.email },
    });
    return user;
  }

  async addDiscount(dto: AddDiscountDto) {
    await this.userRepository.update(
      { discount: dto.discount },
      { where: { email: dto.email } },
    );
    const user = await this.userRepository.findOne({ where: { email: dto.email } });
    return user;
  }
}

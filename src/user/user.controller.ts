import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { RoleAuthGuard } from '../auth/role.auth.guard';
import { ChangeRoleDto } from './dto/change.role.dto';
import { AddBalanceDto } from './dto/add.balance.dto';
import { AddDiscountDto } from "./dto/add.discount.dto";

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  @UseGuards(JwtAuthGuard, RoleAuthGuard)
  getAll() {
    return this.userService.getAll();
  }

  @Post('/role')
  @UseGuards(JwtAuthGuard, RoleAuthGuard)
  changeRole(@Body() dto: ChangeRoleDto) {
    return this.userService.changeRole(dto);
  }

  @Post('/balance')
  @UseGuards(JwtAuthGuard, RoleAuthGuard)
  addBalance(@Body() dto: AddBalanceDto) {
    return this.userService.addBalance(dto);
  }

  @Post('/discount')
  @UseGuards(JwtAuthGuard, RoleAuthGuard)
  addDiscount(@Body() dto: AddDiscountDto) {
    return this.userService.addDiscount(dto);
  }
}

import { forwardRef, Module } from '@nestjs/common';
import { PhoneController } from './phone.controller';
import { PhoneService } from './phone.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { PhoneModel } from './phone.model';
import { AuthModule } from '../auth/auth.module';
import { PhoneOrderModel } from './phone.order.model';
import { UserModule } from '../user/user.module';
import { ProgramModule } from '../program/program.module';
import { BrandModule } from '../brand/brand.module';

@Module({
  controllers: [PhoneController],
  providers: [PhoneService],
  imports: [
    BrandModule,
    SequelizeModule.forFeature([PhoneModel, PhoneOrderModel]),
    AuthModule,
    UserModule,
    ProgramModule,
  ],
  exports: [PhoneService],
})
export class PhoneModule {}

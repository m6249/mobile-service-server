import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { LoginUserDto } from './dto/login.user.dto';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../user/dto/create.user.dto';
import { JwtAuthGuard } from './jwt.auth.guard';
import { RestorePasswordDto } from './dto/restore.password.dto';
import { RoleAuthGuard } from "./role.auth.guard";

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/login')
  login(@Body() dto: LoginUserDto) {
    return this.authService.login(dto);
  }
  @Post('/admin/login')
  admin_login(@Body() dto: LoginUserDto) {
    return this.authService.admin_login(dto);
  }

  @Post('/register')
  register(@Body() dto: CreateUserDto) {
    return this.authService.register(dto);
  }

  @Post('/sent_confirmation_code')
  send_confirmation_code(@Body('email') email) {
    return this.authService.send_confirmation_code(email);
  }

  @Post('/check_confirmation_code')
  check_confirmation_code(@Body() data) {
    return this.authService.check_confirmation_code(data);
  }

  @Get('/check')
  @UseGuards(JwtAuthGuard)
  check(@Req() req) {
    return this.authService.check(req);
  }

  @Get('/admin/check')
  @UseGuards(RoleAuthGuard, JwtAuthGuard)
  adminCheck(@Req() req) {
    return this.authService.check(req);
  }

  @Post('/send_confirmation_code_for_restore')
  send_confirmation_code_for_restore(@Body('email') email) {
    return this.authService.send_confirmation_code_for_restore(email);
  }

  @Post('/restore_password')
  restore_password(@Body() data: RestorePasswordDto) {
    return this.authService.restore_password(data);
  }
}

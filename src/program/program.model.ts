import { Column, DataType, Model, Table } from 'sequelize-typescript';

export interface ProgramCreationAtr {
  brandId: number;
  title: string;
  price: number;
  discount: boolean;
  href?: string | null;
  term: string;
  instructionTitle: string;
  instructionStep: [string];
}

@Table({ tableName: 'programs' })
export class ProgramModel extends Model<ProgramModel, ProgramCreationAtr> {
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
  })
  id: number;

  @Column({ type: DataType.INTEGER, allowNull: false})
  brandId: number;

  @Column({ type: DataType.STRING, allowNull: false })
  title: string;

  @Column({ type: DataType.FLOAT, allowNull: false })
  price: number;

  @Column({ type: DataType.BOOLEAN, allowNull: false })
  discount: boolean;

  @Column({ type: DataType.STRING, defaultValue: null })
  href: string | null;

  @Column({ type: DataType.STRING, defaultValue: '1-10' })
  term: string;

  @Column({ type: DataType.STRING, allowNull: false })
  instructionTitle: string;

  @Column({ type: DataType.JSON, allowNull: false })
  instructionStep: [string];
}

export class AddDiscountDto {
  readonly email: string;
  readonly discount: number;
}

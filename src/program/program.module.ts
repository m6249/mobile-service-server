import { forwardRef, Module } from "@nestjs/common";
import { ProgramController } from './program.controller';
import { ProgramService } from './program.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { ProgramModel } from './program.model';
import { BrandModule } from '../brand/brand.module';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { ProgramOrderModel } from './program.order.model';

@Module({
  controllers: [ProgramController],
  providers: [ProgramService],
  imports: [
    SequelizeModule.forFeature([ProgramModel, ProgramOrderModel]),
    forwardRef(() => BrandModule),
    UserModule,
    AuthModule,
  ],
  exports: [ProgramService],
})
export class ProgramModule {}

import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface ServiceCreationAtr {
  title: string;
  items: [string];
}

@Table({ tableName: 'services' })
export class ServiceModel extends Model<ServiceModel, ServiceCreationAtr> {
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
  })
  id: number;

  @Column({ type: DataType.STRING, allowNull: false })
  title: string;

  @Column({ type: DataType.JSON, allowNull: false })
  items: [string];
}

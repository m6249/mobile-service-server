import { Injectable } from '@nestjs/common';
import { CreatePhoneDto } from './dto/create.phone.dto';
import { InjectModel } from '@nestjs/sequelize';
import { PhoneModel } from './phone.model';
import { CreatePhoneOrderDto } from './dto/create.phone.order.dto';
import { PhoneOrderModel } from './phone.order.model';
import { UserService } from '../user/user.service';
import { AuthService } from '../auth/auth.service';
import { ProgramService } from '../program/program.service';
import { BrandService } from '../brand/brand.service';
import { ChangeStatusOrderedPhoneDto } from '../program/dto/change.status.ordered.phone.dto';

@Injectable()
export class PhoneService {
  constructor(
    @InjectModel(PhoneModel) private phoneRepository: typeof PhoneModel,
    @InjectModel(PhoneOrderModel)
    private phoneOrderRepository: typeof PhoneOrderModel,
    private userService: UserService,
    private authService: AuthService,
    private programService: ProgramService,
    private brandService: BrandService,
  ) {}

  async create(dto: CreatePhoneDto) {
    const phone = await this.phoneRepository.create(dto);
    return phone;
  }

  async delete() {
    return true;
  }

  async getOne(id) {
    const phones = await this.phoneRepository.findOne({ where: { id } });
    return phones;
  }

  async getAllWithBrand() {
    const phoneBrands = await this.brandService.getAllBrand('phone');
    const response = await this.programService.generateResponse(
      phoneBrands,
      this.phoneRepository,
    );
    return response;
  }

  async getOrderedPhones() {
    const orderedPhones = await this.phoneOrderRepository.findAll({
      order: [['id', 'DESC']],
    });
    return orderedPhones;
  }

  async getPhonesByBrandId(id) {
    const phones = await this.phoneRepository.findAll({
      where: { brandId: id },
      order: [['id', 'DESC']],
    });
    return phones;
  }

  async orderPhone(dto: CreatePhoneOrderDto) {
    const user = await this.userService.decrementSum(dto.userId, dto.price);
    const orderPhone = await this.phoneOrderRepository.create(dto);
    const { token } = await this.authService.generateToken(user);

    return {
      orderPhone: orderPhone,
      token,
    };
  }

  async getMyPhones(id) {
    const myPhones = await this.phoneOrderRepository.findAll({
      where: { userId: id },
      order: [['id', 'DESC']],
    });
    return myPhones;
  }

  async updatePhone(dto: CreatePhoneDto) {
    await this.phoneRepository.update({ ...dto }, { where: { id: dto.id } });
    const phone = await this.phoneRepository.findOne({
      where: { id: dto.id },
    });
    return phone;
  }

  async deletePhone(id) {
    if (id) {
      const phone = await this.phoneRepository.destroy({ where: { id } });
      return phone;
    }
  }

  async changeStatus(dto: ChangeStatusOrderedPhoneDto) {
    if (dto.status === 'REJECTED') {
      await this.userService.incrementSum(dto.userId, dto.price);
    }
    await this.phoneOrderRepository.update(
      { status: dto.status },
      { where: { id: dto.id } },
    );
    const phone = this.phoneOrderRepository.findOne({ where: { id: dto.id } });
    return phone;
  }

  async getIncome() {
    const programPrices = await this.phoneOrderRepository.findAll({
      attributes: ['price', 'status'],
    });
    let income = 0;
    await programPrices.forEach((value) => {
      income += value.status === 'SUCCESS' ? value.price : 0;
    });
    return income;
  }
}

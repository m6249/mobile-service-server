import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface BrandCreationAtr {
  name: string;
}

@Table({ tableName: 'phoneBrands' })
export class PhoneBrandModel extends Model<PhoneBrandModel, BrandCreationAtr> {
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING, allowNull: false, unique: true })
  name: string;
}

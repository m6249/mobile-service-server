import { LoginUserDto } from './dto/login.user.dto';
import { CreateUserDto } from '../user/dto/create.user.dto';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { UserModel } from '../user/user.model';
import { MailerService } from '@nestjs-modules/mailer';
import { RestorePasswordDto } from './dto/restore.password.dto';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private mailerService: MailerService,
  ) {}

  async login(dto: LoginUserDto) {
    const user = await this.validateUser(dto);
    return this.generateToken(user);
  }

  async admin_login(dto: LoginUserDto) {
    const user = await this.validateUser(dto);
    if (user.role !== 1) {
      throw new HttpException('access_closed', HttpStatus.NOT_FOUND);
    }
    return this.generateToken(user);
  }

  async register(dto: CreateUserDto) {
    const candidate = await this.userService.getUserByEmail(dto.email);
    if (candidate) {
      throw new HttpException(
        'user_with_such_email_exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    const hashPassword = await bcrypt.hash(dto.password, 5);
    const user = await this.userService.create({
      ...dto,
      password: hashPassword,
    });
    return this.generateToken(user);
  }

  async check_confirmation_code(data) {
    const checkCode = await bcrypt.compare(data.code, data.random_code_cache);
    if (checkCode) {
      return {
        message: 'code_matches',
      };
    }
    return {
      message: 'code_does_not_match',
    };
  }

  async send_confirmation_code(email) {
    const candidate = await this.userService.getUserByEmail(email);
    if (candidate) {
      throw new HttpException(
        'user_with_such_email_exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.sendVerificationCode(email);
  }

  async send_confirmation_code_for_restore(email) {
    const candidate = await this.userService.getUserByEmail(email);
    if (!candidate) {
      throw new HttpException('user_is_not_found', HttpStatus.BAD_REQUEST);
    }
    return this.sendVerificationCode(email);
  }

  async restore_password(data: RestorePasswordDto) {
    const hashPassword = await bcrypt.hash(`${data.password}`, 5);
    const user = await this.userService.restorePassword(data, hashPassword);
    const token = await this.generateToken(user);
    return token;
  }

  async check(req) {
    const user = await this.userService.getUserByEmail(req.user.email);
    const { token } = await this.generateToken(user);
    return {
      message: 'user_is_authorized',
      token,
    };
  }

  async generateToken(user: UserModel) {
    const payload = {
      id: user.id,
      email: user.email,
      fullName: user.fullName,
      phoneNumber: user.phoneNumber,
      sum: user.sum,
      spentMoney: user.spentMoney,
      country: user.country,
      role: user.role,
      discount: user.discount,
    };
    return {
      token: this.jwtService.sign(payload),
    };
  }

  async validateUser(dto: LoginUserDto) {
    const candidate = await this.userService.getUserByEmail(dto.email);
    if (!candidate) {
      throw new HttpException('user_is_not_found', HttpStatus.BAD_REQUEST);
    }
    const checkPassword = await bcrypt.compare(
      dto.password,
      candidate.password,
    );
    if (checkPassword) {
      return candidate;
    }
    throw new HttpException('password_does_not_match', HttpStatus.BAD_REQUEST);
  }

  async sendVerificationCode(email) {
    const random_code =
      Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;
    const random_code_cache = await bcrypt.hash(`${random_code}`, 5);

    await this.mailerService.sendMail({
      from: process.env.EMAIL,
      to: email,
      subject: 'Mobile service',
      html: `
    <h2>Код подтверждение:</h2>
    <h1 align="center" style="color: red;">${random_code}</h1> 
    `,
    });
    return {
      random_code_cache,
    };
  }
}

import { Module } from '@nestjs/common';
import { SumService } from './sum.service';
import { SumController } from './sum.controller';
import { ProgramModule } from '../program/program.module';
import { PhoneModule } from '../phone/phone.module';
import { GameModule } from '../game/game.module';

@Module({
  providers: [SumService],
  controllers: [SumController],
  imports: [ProgramModule, PhoneModule, GameModule],
})
export class SumModule {}

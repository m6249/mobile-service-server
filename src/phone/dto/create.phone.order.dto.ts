import { CreatePhoneDto } from './create.phone.dto';
import { CreateUserDto } from '../../user/dto/create.user.dto';

export class CreatePhoneOrderDto {
  readonly phone: CreatePhoneDto;
  readonly userId: number;
  readonly user: CreateUserDto;
  readonly sn: string;
  readonly price: number;
  readonly status: string;
}

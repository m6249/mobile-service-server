import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { BrandService } from './brand.service';
import { CreateBrandDto } from './dto/create.brand.dto';
import { RoleAuthGuard } from '../auth/role.auth.guard';
import { DeleteBrandDto } from './dto/delete.brand.dto';

@Controller('/brand')
export class BrandController {
  constructor(private brandService: BrandService) {}

  @Post()
  @UseGuards(RoleAuthGuard)
  createBrand(@Body() dto: CreateBrandDto) {
    return this.brandService.createBrand(dto);
  }

  @Get()
  getAllBrands(@Query('type') type: string) {
    return this.brandService.getAllBrand(type);
  }

  @Post('/delete')
  deleteBrand(@Body() dto: DeleteBrandDto) {
    return this.brandService.deleteBrand(dto);
  }
}

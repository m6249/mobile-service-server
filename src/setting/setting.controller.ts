import { Body, Controller, Get, Post } from '@nestjs/common';
import { SettingService } from './setting.service';
import { CreateServiceDto } from './dto/create.service.dto';

@Controller('setting')
export class SettingController {
  constructor(private settingService: SettingService) {}
  @Post('/service')
  create(@Body() dto: CreateServiceDto) {
    return this.settingService.create(dto);
  }

  @Get('/service')
  getAll() {
    return this.settingService.getAll();
  }

  @Post('/service/delete')
  delete(@Body('id') id: number) {
    return this.settingService.deleteService(id);
  }
}

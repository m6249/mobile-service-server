export class AddBalanceDto {
  readonly email: string;
  readonly balance: number;
}
import { Column, DataType, Model, Table } from 'sequelize-typescript';
import { CreateGameDto } from './dto/create.game.dto';
import { CreateUserDto } from '../user/dto/create.user.dto';

interface GameOrderCreationAtr {
  readonly game: CreateGameDto;
  readonly userId: number;
  readonly user: CreateUserDto;
  readonly playerId: string;
  readonly nickName?: string;
  readonly price: number;
  readonly status: string;
}

@Table({ tableName: 'gameOrders' })
export class GameOrderModel extends Model<
  GameOrderModel,
  GameOrderCreationAtr
> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @Column({ type: DataType.JSON, allowNull: false })
  game: CreateGameDto;

  @Column({ type: DataType.INTEGER, allowNull: false })
  userId: number;

  @Column({ type: DataType.JSON, allowNull: false })
  user: CreateUserDto;

  @Column({ type: DataType.STRING, allowNull: false })
  playerId: string;

  @Column({ type: DataType.FLOAT, allowNull: false })
  price: number;

  @Column({ type: DataType.STRING })
  nickName: string;

  @Column({ type: DataType.STRING, defaultValue: 'PENDING' })
  status: string;
}

import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class RoleAuthGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    try {
      const authHeader = req.headers.authorization;
      const token = authHeader.split(' ')[1];
      const user = this.jwtService.verify(token);
      console.log(user.role);
      if (user.role !== 1) {
        throw new UnauthorizedException({ message: 'user_is_not_admin' });
      }
      return true;
    } catch (e) {
      console.log(e);
      throw new UnauthorizedException({ message: 'user_is_not_admin' });
    }
  }
}
